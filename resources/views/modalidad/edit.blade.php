@extends('welcome')
@section('titulo','Editar modalidad de convenio')

@section('contenido')

<div class="principal">
            <h1 class="titleform">Editar modalidad de convenio</h1>
            <div class="linetitle"></div>
            {{ Form::open(['route' => ['modalidad.update',$estado],  'method' => 'PUT','class'=>'formulario']) }}            
            <div class="form-2">
                {{ Form::label('nombre_modalidad_convenio', 'Nombre de modalidad de convenio',['class'=>'form-label']) }}
                {{ Form::text('nombre_modalidad_convenio', $estado->nombre_estado_convenio, ['class' => 'form-text'])}}
            </div>               

            <div class="form-2">
                {{ Form::label('descripcion', 'Descripcion',['class'=>'form-label']) }}
                {{ Form::text('descripcion', $estado->descripcion, ['class' => 'form-text'])}}
            </div>

            <div class="form-2">
                {{ Form::label('vigente', 'Vigente',['class'=>'form-label']) }}
                {{ Form::select('vigente', ['1' => 'Si', '0' => 'No'], $estado->vigente, ['class' => 'form-text', 'placeholder' => 'Seleccione una opción']) }}                
            </div>

            <div class="form-group">
                {{ Form::submit('Guardar',['class'=>'btn-submit']) }}
            </div>
                    {{ Form::close()}}
                </div>
            </div>            
</div>
@endsection