@extends('welcome')

@section('contenido')
<div class="principal">
	<h1 class="titleform">Ingrese sus datos</h1>
	<div class="linetitle"></div>
	@if ($errors->has('email'))
	<span class="help-block">
		<strong>{{ $errors->first('email') }}</strong>
	</span>
	@endif
	{{Form::open(['url'=> '/login', 'method' => 'POST', 'class'=>'formulario'])}}

	<div class="form-2">
		<label for="email" class="form-label">Email</label>
		<input id="email" type="email" class="form-text" name="email" value="{{ old('email') }}">      
	</div>
	<div class="form-2">
		<label for="password" class="form-label">Contraseña</label>
		<input id="password" type="password" class="form-text" name="password">
	</div>

	@if ($errors->has('password'))
	<span class="help-block">
		<strong>{{ $errors->first('password') }}</strong>
	</span>
	@endif





	<button type="submit" class="btn-submit">
		<i class="fa fa-btn fa-sign-in"></i> Ingresar
	</button>


	{{Form::close()}}
</div>        
@endsection
