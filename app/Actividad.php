<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Actividad extends Model
{
    protected $table = 'actividades';
    protected $fillable = ['nombre_actividad', 'descripcion','fecha_inicio','fecha_fin','vigente'];
}
