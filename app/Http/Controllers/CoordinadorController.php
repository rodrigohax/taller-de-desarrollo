<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Coordinador;
use App\Http\Middleware\EsCoordinador;
class CoordinadorController extends Controller
{
        public function __construct()
    {

        $this->middleware('administrador');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coordinadores = Coordinador::orderBy('id', 'ASC')->paginate(5);
        return view('coordinador.index')->with('coordinadores',$coordinadores);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('coordinador.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $coordinador = new Coordinador($request->all());
        if($request->fecha_fin==null){
            $coordinador->vigente=true;
        }
        
        if($request->esExterno=="Sí"){
            $coordinador->esExterno=true;
        }else{
            $coordinador->esExterno=false;
        }
        $coordinador->save();
        //Flash::success("Se ha registrado el coordinador " . $coordinador->nombre);
        return redirect()->route('coordinador.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $coordinador = Coordinador::find($id);
        return view('coordinador.edit')->with('coordinador', $coordinador);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $coordinador = Coordinador::find($id);
        $coordinador->fill($request->all());
        $coordinador->save();
        //Flash::success("Se ha actualizado el coordinador " . $coordinador->nombre);
        return redirect()->route('coordinador.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
            Coordinador::destroy($id);
           // Flash::success("Se ha eliminado correctamente");
            return redirect()->route('coordinador.index');
    }
}
