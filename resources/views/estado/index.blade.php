@extends('welcome')
@section('contenido')
<div class="principal">            
            <div class="div-group-3-btn">
                <a href="{{ route('estado.create') }}" class="btn-principal">Agregar estado</a>
            </div>
            <h1 class="titleform">Estados de convenio</h1>
            <div class="linetitle"></div>
    <br><br>
    <table class="table">
                    <thead>
                        <tr class="encabezado-tabla">
                            <th>ID</th>
                            <th>Nombre de estado de convenio</th>
                            <th>Descripcion</th>
                            <th>Vigente</th>
                            <th>Editar</th>   
                            <th>Eliminar</th>   
                        </tr> 
                    </thead>
                    <tbody>
                    @if(count($estados)>0)
                        @foreach($estados as $estado)
                            <tr class="table-color">
                                <td class="td-tabla">{{$estado->id}}</td>                               
                                <td class="td-tabla">{{$estado->nombre_estado_convenio}}</td>
                                <td class="td-tabla">{{$estado->descripcion}}</td>
                                @if($estado->vigente==1)
                                    <td class="td-tabla">Sí</td>
                                @else
                                    <td class="td-tabla">No</td>
                                @endif                               

                                <td class="td-tabla"><a href="{{ route('estado.edit', $estado->id) }}" class="btn btn-warning">
                                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>

                                <td class="td-tabla"><a href="{{ route('estado.destroy', $estado->id) }}" class="btn btn-danger" onclick="return confirm('Eliminar')"> <span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>
                            </tr>
                            @endforeach
                    @else
                    <div class="tabla-vacia">
                        <p>Sin convenios</p>
                    </div>
                    
                    @endif
                    </tbody>
        </table>

</div>
@endsection
