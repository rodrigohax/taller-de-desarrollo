<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\ModalidadConvenio;

class ModalidadConvenioController extends Controller
{

    public function __construct()
    {

        $this->middleware('auth', ['only' => [
            'index',
        ]]);

        $this->middleware('administrador', ['only' => [
            'edit',
            'destroy',
            'create',
        ]]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modalidad = ModalidadConvenio::orderBy('id', 'ASC')->paginate(10);
        return view('modalidad.index')->with('modalidades',$modalidad);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('modalidad.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $modalidad = new ModalidadConvenio($request->all());       
        $modalidad->save();
        //Flash::success("Se ha registrado el modalidad con id" . $modalidad->id);
        return redirect()->route('modalidad.index');       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $modalidad = ModalidadConvenio::find($id);
        return view('modalidad.edit')->with('modalidad', $modalidad);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $modalidad= ModalidadConvenio::find($id);
        $modalidad->fill($request->all());
        $modalidad->save();
        return redirect()->route('modalidad.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ModalidadConvenio::destroy($id);
        return redirect()->route('modalidad.index');
    }
}
