<?php

namespace App\Http\Middleware;

use Closure;

class EsUsuario
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->check() && auth()->user()->rol=="usuario") {
            return $next($request);
        }
        return redirect('/')->withErrors(['auth' => 'Acceso Denegado',]);
    }
}
