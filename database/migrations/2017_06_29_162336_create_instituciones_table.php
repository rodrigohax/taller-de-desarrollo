<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstitucionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instituciones',function(Blueprint $table){

            $table->increments('id');
            $table->timestamps();
            $table->integer('id_tipo_institucion')->unsigned()->index();
            $table->string('nombre_institucion');
            $table->integer('id_pais')->unsigned()->index();
            $table->boolean('vigente');
            $table->char('id_internacional');
            $table->string('rut_institucion');
            $table->string('razon_social_institucion');
            $table->string('direccion_institucion');
            $table->string('telefono_institucion');
            $table->string('email_institucion');
            $table->string('link_institucion');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('instituciones');
    }
}
