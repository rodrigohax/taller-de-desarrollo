@extends('welcome')
@section('contenido')
@section('titulo','Coordinadores')
<div class="principal">            
     
            <a href="{{ route('coordinador.create') }}" class="btn-principal" style="float: right;">Agregar coordinador</a>
           <h1 class="titleform" style=" margin-top: 1%;">Coordinadores</h1>
            <div class="linetitle" style=" margin-top: 2%;"></div>

    <br><br>    <table class="table">
                    <thead>
                        <tr class="encabezado-tabla">
                            <th>ID</th>     
                            <th>Rut</th>
                            <th>Nombre</th>
                            <th>Email</th>
                            <th>Fecha inicio</th>
                            <th>Fecha termino</th>
                            <th>Vigente</th>
                            <th>Externo</th>                 
                            <th>Unidad académica</th>   
                            <th></th>   
                            <th></th>
                            <th></th>
                            
                        </tr> 
                    </thead>
                    <tbody>
                    @if(count($coordinadores)>0)
                        @foreach($coordinadores as $coordinador)
                            <tr class="table-color">
                                <td class="td-tabla">{{$coordinador->id}}</td>
                                <td class="td-tabla">{{$coordinador->rut}}</td>
                                <td class="td-tabla">{{$coordinador->nombre}}</td>
                                <td class="td-tabla">{{$coordinador->email}}</td>
                                <td class="td-tabla">{{date("d/m/Y",strtotime($coordinador->fecha_inicio))}}</td>
                                @if($coordinador->fecha_fin==null || $coordinador->fecha_fin=='0000-00-00')
                                    <td class="td-tabla"> -</td>
                                @else
                                    <td class="td-tabla">{{date("d/m/Y",strtotime($coordinador->fecha_fin))}}</td>
                                @endif
                                @if($coordinador->vigente==1)
                                    <td class="td-tabla">Sí</td>
                                @else
                                    <td class="td-tabla">No</td>
                                @endif
                                @if($coordinador->esExterno==1)
                                    <td class="td-tabla">Sí</td>
                                @else
                                    <td class="td-tabla">No</td>
                                @endif
                                <td class="td-tabla">{{$coordinador->unidad_academica}}</td>
                        

                                <td class="td-tabla"><a href="{{ route('coordinador.edit', $coordinador->id) }}" class="btn btn-warning">
                                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>

                                <td class="td-tabla"><a href="{{ route('coordinador.destroy', $coordinador->id) }}" class="btn btn-danger" onclick="return confirm('Eliminar')"> <span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>
                            </tr>
                            @endforeach
                    @else
                    <div class="tabla-vacia">
                        <p>Sin coordinadores</p>
                    </div>
                    
                    @endif
                    </tbody>
        </table>

                        
                    
                
       
</div>
@endsection
