<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('institucion', ['middleware' => 'auth', 'uses' => 'InstitucionController@index', 'as' => 'institucion.index']);
Route::get('institucion/create',['middleware' => 'administrador', 'uses'=>'InstitucionController@create', 'as' => 'institucion.create']);

Route::get('institucion/{id}/edit', ['middleware' => 'coordinador', 'middleware' => 'administrador','uses' => 'InstitucionController@edit', 'as' => 'institucion.edit']);

Route::get('institucion/{id}', ['middleware' => 'auth', 'uses' => 'InstitucionController@show', 'as' => 'institucion.show']);

Route::post('institucion', ['uses' => 'InstitucionController@store', 'as' => 'institucion.store']);
Route::patch('institucion/{id}', ['uses' => 'InstitucionController@update', 'as' => 'institucion.update']);
Route::put('institucion/{id}', [
    'as' => 'institucion.update', 'uses' => 'InstitucionController@update'
]);
Route::get('instituciondestro/{id}/',['middleware' => 'administrador', 'uses' => 'InstitucionController@destroy']);


Route::resource('actividad', 'ActividadController');
Route::resource('coordinador', 'CoordinadorController');

Route::get('actividad/{id}/destroy', ['uses' => 'ActividadController@destroy', 'as' => 'actividad.destroy']);

Route::get('coordinador/{id}/destroy', ['uses' => 'CoordinadorController@destroy', 'as' => 'coordinador.destroy']);


Route::resource('convenio', 'ConvenioController');
Route::auth();

/*Solo el admin puede hacer estas acciones*/
Route::get('convenio/{id}/destroy', ['uses' => 'ConvenioController@destroy', 'as' => 'convenio.destroy']);
Route::resource('modalidad', 'ModalidadConvenioController');
Route::get('modalidad/{id}/destroy', ['uses' => 'ModalidadConvenioController@destroy', 'as' => 'modalidad.destroy']);
Route::resource('estado', 'EstadoConvenioController');
Route::get('estado/{id}/destroy', ['uses' => 'EstadoConvenioController@destroy', 'as' => 'estado.destroy']);

