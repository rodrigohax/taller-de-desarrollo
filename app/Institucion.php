<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Institucion extends Model
{
    public $table= 'instituciones';

    protected $fillable = ["id_institucion","id_tipo_institucion","nombre_institucion",
        "id_pais","vigente","id_internacional","rut_institucion","razon_social_institucion",
        "direccion_institucion","telefono_institucion","email_institucion","link_institucion"];

}
