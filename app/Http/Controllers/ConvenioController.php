<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use App\Convenio;
use Illuminate\Support\Facades\Session;

class ConvenioController extends Controller
{
        public function __construct()
    {

        $this->middleware('auth', ['only' => [
            'index',
        ]]);

        $this->middleware('administrador', ['only' => [
            'edit',
            'destroy',
            'create',
        ]]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $convenio = Convenio::orderBy('id', 'ASC')->paginate(10);
        return view('convenio.index')->with('convenios',$convenio);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $instituciones = DB::table('instituciones')->lists('nombre_institucion','id');
        $modalidades = DB::table('modalidadConvenios')->lists('nombre_modalidad_convenio','id');
        $estados = DB::table('estadoConvenios')->lists('nombre_estado_convenio','id');
        return view('convenio.create')->with('instituciones',$instituciones)
            ->with('modalidades',$modalidades)->with('estados', $estados);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $convenio = new Convenio($request->all());             
        $convenio->id_estado_convenio = $request->idEstados;
        $convenio->id_tipo_convenio = $request->idModalidades;
        $convenio->save();
        if(count($request->id_instituciones)>0){
            foreach($request->id_instituciones as $institucion){
                DB::table('convenio_institucion')->insert(['id_convenio'=>$convenio->id, 'id_institucion'=>$institucion]);
            }    
        }      

        Session::flash('mensaje',"Se ha registrado el convenio con id" . $convenio->id);
        return redirect()->route('convenio.index');       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $convenio = Convenio::find($id);
        $convenio_institucion=DB::table('convenio_institucion')->join('instituciones','instituciones.id','=','convenio_institucion.id_institucion')->where('id_convenio',$id)->select('instituciones.nombre_institucion')->get();
        
        return view('convenio.show')->with('convenio', $convenio)->with('convenio_institucion',$convenio_institucion);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $convenio = Convenio::find($id);
        $instituciones = DB::table('instituciones')->lists('nombre_institucion','id');
    $modalidades = DB::table('modalidadConvenios')->lists('nombre_modalidad_convenio','id');
        $estados = DB::table('estadoConvenios')->lists('nombre_estado_convenio','id');
            $convenio_institucion=DB::table('convenio_institucion')->join('instituciones','instituciones.id','=','convenio_institucion.id_institucion')->where('id_convenio',$id)->select('instituciones.nombre_institucion','instituciones.id')->get();
        
            return view('convenio.edit')->with('convenio', $convenio)->with('convenio_institucion',$convenio_institucion)->with('instituciones',$instituciones)->with('modalidades',$modalidades)->with('estados', $estados);;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $convenio= Convenio::find($id);
        $convenio->fill($request->all());
        $convenio->save();

        DB::table('convenio_institucion')->where('id_convenio',$id)->delete();
        if(count($request->id_instituciones)>0){
            foreach($request->id_instituciones as $institucion){
                DB::table('convenio_institucion')->insert(['id_convenio'=>$convenio->id, 'id_institucion'=>$institucion]);
            }
        }
        return redirect()->route('convenio.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('convenio_institucion')->where('id_convenio',$id)->delete();
        Convenio::destroy($id);
        
        return redirect()->route('convenio.index');
    }
}
