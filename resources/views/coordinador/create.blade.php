@extends('welcome')
@section('titulo','Registro de actividad')
@section('contenido')
<div class="principal">
            <h1 class="titleform">Registro de coordinador</h1>
            <div class="linetitle"></div>
        {{ Form::open(['route' => 'coordinador.store',  'method' => 'POST','class'=>'formulario']) }}
            <div class="form-2">
                {{ Form::label('rut', 'Rut',['class'=>'form-label']) }}
                {{ Form::text('rut', null, ['class' => 'form-text'])}}
            </div>
            <div class="form-2">
                {{ Form::label('nombre', 'Nombre',['class'=>'form-label']) }}
                {{ Form::text('nombre', null, ['class' => 'form-text'])}}
            </div>
            <div class="form-2">
                {{ Form::label('unidad_academica', 'Unidad Académica: ',['class'=>'form-label']) }}
                {{ Form::text('unidad_academica', null, ['class' => 'form-text'])}}
            </div>
            <div class="form-2">
                {{ Form::label('email', 'Email',['class'=>'form-label']) }}
                {{ Form::email('email', null, ['class' => 'form-text'])}}
            </div>
            <div class="form-2">
                {{ Form::label('fecha_inicio', 'Fecha inicio',['class'=>'form-label']) }}
                {{ Form::date('fecha_inicio', null, ['class' => 'form-text'])}}
            </div>
                        
            <div class="form-2">
                {{ Form::label('fecha_fin', 'Fecha término',['class'=>'form-label']) }}
                {{ Form::date('fecha_fin', null, ['class' => 'form-text']) }}
            </div>
            <div class="form-2">
                {{Form::label('esExterno','Externo:', ['class'=>'form-label'])}}
                <div class="radiobuttons">
                    <div class="rb">
                        {{Form::radio('esExterno','Sí')}}
                        <label for="radio01"><span></span>Sí</label>        
                    </div>
                    <div class="rb">
                        {{Form::radio('esExterno','No')}}
                          <label for="radio01"><span></span>No</label>      
                    </div>
                </div>
            </div>
            <div class="form-group">
                {{ Form::submit('Guardar',['class'=>'btn-submit']) }}
            </div>
                    {{ Form::close()}}
                </div>
            </div>
  
</div>
@endsection