@extends('welcome')
@section('contenido')

    @if(Session::get('mensaje'))
        <div class='alert alert-success'>
            <p><strong>{!! Session::get('mensaje') !!}</strong></p>
        </div>
    @endif

    <div class="principal">


    <a href="{{ route('institucion.create') }}" class="btn-principal" role="button" style="float:right">Agregar Institucion</a>
         <h1 class="titleform" style=" margin-top: 1%;">Instituciones</h1>



      
    


<br>


        <div class="linetitle"></div>
        <br><br>
     
        <table class="table">
            <thead>
            <tr class="encabezado-tabla">
                <th>ID</th>
                <th>Nombre Institucion</th>
                <th>Rut Institucion</th>
                <th>Direccion institucion</th>
                <th>Email</th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @if(count($instituciones)>0)
                @foreach($instituciones as $institucion)

                    <tr>

                        <td class="td-tabla">{{$institucion->id}}</td>

                        <td class="td-tabla">{{$institucion->nombre_institucion}}</td>
                        <td class="td-tabla">{{$institucion->rut_institucion}}</td>
                        <td class="td-tabla">{{$institucion->direccion_institucion}}</td>
                        <td class="td-tabla">{{$institucion->email_institucion}}</td>



                        <td class="td-tabla"><a href="{{ route('institucion.show', $institucion->id) }}" class="btn btn-info">
                                <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>

                        <td class="td-tabla"><a href="{{ route('institucion.edit', $institucion->id) }}" class="btn btn-warning">
                                <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>

                        <td class="td-tabla"> <a class="btn btn-danger" href="{!! URL::to('instituciondestro/'.$institucion->id )!!}}" ><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>
                    </tr>
                @endforeach
            @else
                <div class="tabla-vacia">
                    <p>Sin instituciones</p>
                </div>

            @endif
            </tbody>
        </table>
        </div>


@endsection