<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Convenio extends Model
{
    protected $table = 'convenios';
    protected $fillable = ['id','id_tipo_convenio','id_coordinador_convenio','id_estado_convenio','nombre_convenio','fecha_inicio','fecha_fin','fecha_firma','fecha_decreto','numero_decreto','descripcion','vigente','vigencia'];
}
