<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConveniosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('convenios', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('id_tipo_convenio')->unsigned()->index();
            $table->integer('id_coordinador_convenio')->unsigned()->index();
            $table->integer('id_estado_convenio')->unsigned()->index();
            $table->string('nombre_convenio');
            $table->date('fecha_inicio');
            $table->date('fecha_fin');
            $table->date('fecha_firma');
            $table->date('fecha_decreto');
            $table->integer('numero_decreto');
            $table->string('descripcion');
            $table->boolean('vigente');
            $table->integer('vigencia');
        });

        Schema::create('convenio_institucion',function(Blueprint $table){
            $table->integer('id_convenio')->unsigned();
            $table->integer('id_institucion')->unsigned();

            $table->foreign('id_convenio')->references('id')->on('convenios');
            $table->foreign('id_institucion')->references('id')->on('instituciones');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::drop('convenios');
    }
}
