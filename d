[33mcommit dfc2c949e090e71eaca6e1b5c11b73db31073c73[m
Author: Rodrigo <rodrigohax@gmail.com>
Date:   Fri Jul 7 17:54:17 2017 +0000

    .gitignore editado

[33mcommit 40895580f534e3a1e56c1ad6ed4b378a2dcccc2c[m
Author: karla_leal <karla.leal1401@alumnos.ubiobio.cl>
Date:   Fri Jul 7 12:35:17 2017 -0400

    Validaciones listas, crud funcionando. Solo falta mezclar el sistema, con la tabla convenios.

[33mcommit c40bf9e0eaef47664a38eebfe5b52e0d7a8476c2[m
Merge: 52051d0 c785a79
Author: Dario Jofre <dario.jofrev@gmail.com>
Date:   Fri Jul 7 01:58:03 2017 -0400

    se agrega vistas de crud coordinadores y se arreglan otros detalles

[33mcommit 52051d0f26905e47146c6d75f5e99a31f2a0d8b7[m
Author: Dario Jofre <dario.jofrev@gmail.com>
Date:   Fri Jul 7 01:28:38 2017 -0400

    Vista de tabla coordinadores lista, falta create y edit

[33mcommit a997a20325998f199453f0968c435a7e7dd5ba80[m
Author: Dario Jofre <dario.jofrev@gmail.com>
Date:   Fri Jul 7 01:21:26 2017 -0400

    Vista de tabla coordinadores lista, falta create y edit

[33mcommit 4a6c0709fc68b5d29c05a0c19d0534c46f63fd57[m
Merge: 1da5d07 c45d397
Author: Rodrigo <rodrigohax@gmail.com>
Date:   Fri Jul 7 00:23:31 2017 -0400

    Merge con coordinadores, faltan vistas

[33mcommit c785a79e0cf928ac2c7ea066f4ec78da573a9e15[m
Author: Dario Jofre <dario.jofrev@gmail.com>
Date:   Fri Jul 7 00:22:15 2017 -0400

    Arreglo de vista index actividades

[33mcommit 633a6c57bdeab33a11a09dbfd642004d11b8e2d3[m
Author: Rodrigo <rodrigohax@gmail.com>
Date:   Thu Jul 6 23:22:46 2017 +0000

    Login creado, falta arreglar vistas

[33mcommit f03ce527810f4c195698b75557000a2c50ad3e24[m
Author: karla_leal <karla.leal1401@alumnos.ubiobio.cl>
Date:   Wed Jul 5 12:52:11 2017 -0400

    Todo funciona a excepcion de las validaciones.

[33mcommit 56d56d60c68851d05b4a9c6520e3e2f2f2329dbc[m
Author: nicoyarce <nicoyarce@gmail.com>
Date:   Tue Jul 4 22:07:31 2017 -0400

    Crud de convenios sin validaciones, faltan relaciones entre tablas

[33mcommit c2a3719900a10a58b8df732cca344ff4a1a9479e[m
Author: Karla <karla.leal1401@alumnos.ubiobio.cl>
Date:   Tue Jul 4 15:15:43 2017 -0400

    bootstrap y cambios al index y al create, cree tambien el request de
    institucion

[33mcommit 2b0e0aa8105ed364dab5b9ac6b8e3cf43801f24e[m
Author: Karla <karla.leal1401@alumnos.ubiobio.cl>
Date:   Mon Jul 3 17:08:43 2017 -0400

    bootstrap y cambios al index y al create

[33mcommit 1da5d070ec1d324fad1d4fd489d9bb9d219d45e1[m
Author: Dario Jofre <dario.jofrev@gmail.com>
Date:   Fri Jun 30 11:02:42 2017 -0400

    Se agrega etiqueta actividades

[33mcommit 22cb8349f3df39ef1ebf4cd9cd8e88ab5ea4d029[m
Author: Dario Jofre <dario.jofrev@gmail.com>
Date:   Fri Jun 30 01:51:50 2017 -0400

    Se crea el edición y ver informacion para las actividades, se sigue realizando las vistas con la plantilla creada

[33mcommit 78da4e51f33733e8b9c66afee6e9546c06912433[m
Merge: 1013e84 e92eb8b
Author: Dario Jofre <dario.jofrev@gmail.com>
Date:   Fri Jun 30 01:32:08 2017 -0400

    Se arreglar y modifican el controlador y la vista para crear actividad, se modifica y adapta a la plantilla base la tabla y formularios

[33mcommit 1013e840b292505ede6400f2e6e03f02a969a1ec[m
Author: Dario Jofre <dario.jofrev@gmail.com>
Date:   Fri Jun 30 01:27:24 2017 -0400

    Integracion de plantilla a tabla y vista create de actividades, se deja plantilla base en otra vista

[33mcommit 95da95a55044dc9f25dacf565b333c2ba4d92f1c[m
Author: nicoyarce <nicoyarce@gmail.com>
Date:   Fri Jun 30 03:26:34 2017 +0000

    Agregadas vistas de crear y mostrar actividades

[33mcommit 40be37d5f79a2c7d9f5f4c03437b5993a974dafe[m
Merge: 9245e27 7b856f4
Author: nicoyarce <nicoyarce@gmail.com>
Date:   Fri Jun 30 01:39:44 2017 +0000

    Merge branch 'nicolas'

[33mcommit 7b856f4ebf38ec19c33ec5b0c6979f6fbdbea9dd[m
Author: nicoyarce <nicoyarce@gmail.com>
Date:   Fri Jun 30 01:30:32 2017 +0000

    Avances en vistas y arreglos en controladores de actividad

[33mcommit 45400e47876916bb27a0de9e215d54c7b060ec06[m
Author: Karla Leal <karla.leal1401@alumnos.ubiobio.cl>
Date:   Thu Jun 29 18:15:48 2017 -0400

    Funcionalidad para crear e indexar instituciones.

[33mcommit 9245e27fa4f61299a472e635f35afcc89f395de8[m
Author: Dario Jofre <dario.jofrev@gmail.com>
Date:   Thu Jun 29 18:08:46 2017 -0400

    Plantilla completa subida al  master

[33mcommit e92eb8bd94f7d201e1ce724f179869ab93ddcd89[m
Author: Dario Jofre <dario.jofrev@gmail.com>
Date:   Thu Jun 29 18:03:11 2017 -0400

    cambio color de fondo

[33mcommit 3113f8d7364d3f9edaefcb2a3058ec63c6b97d55[m
Author: Dario Jofre <dario.jofrev@gmail.com>
Date:   Thu Jun 29 17:57:59 2017 -0400

    Terminado diseño de botones submit y principales, dropdown terminados listo para uso

[33mcommit 0fb4b103dc00ae0e4b02f5b1fca819336f9f8130[m
Author: Dario Jofre <dario.jofrev@gmail.com>
Date:   Thu Jun 29 16:46:30 2017 -0400

    Se agrega laravelcollective para formularios

[33mcommit e1b2aabfae1e01f2d1ecf2db345cd1d63d8ad2ba[m
Author: Dario Jofre <dario.jofrev@gmail.com>
Date:   Thu Jun 29 16:30:35 2017 -0400

    Archivo composer.lock modificado

[33mcommit b0a7083d25290f69df79c5b2351b70c27811776b[m
Author: karla_leal <karla.leal1401@alumnos.ubiobio.cl>
Date:   Thu Jun 29 20:15:48 2017 +0000

    Flash y Laravelcollective agregados

[33mcommit 697afb3b781c6f5a63b3fd06c1531246a92f6d12[m
Merge: d67c91f 123966b
Author: karla_leal <karla.leal1401@alumnos.ubiobio.cl>
Date:   Thu Jun 29 19:50:58 2017 +0000

    Merge branch 'karla'

[33mcommit 123966b31425d6f8c7bd28032161439bfa8ef5c5[m
Author: karla_leal <karla.leal1401@alumnos.ubiobio.cl>
Date:   Thu Jun 29 17:47:46 2017 +0000

    Se acaban de agregar las vistas que se utilizaran

[33mcommit feeaadc1e233a879ee25bad64b8f4b6c9bb228a7[m
Author: vagrant <vagrant@homestead>
Date:   Thu Jun 29 17:47:28 2017 +0000

    Completado el controlador, y creacion de archivos de vistas

[33mcommit c45d397ff87d090f42191c0c69fec2876de0deae[m
Author: Rodrigo <rodrigohax@gmail.com>
Date:   Thu Jun 29 17:37:42 2017 +0000

    Creacion controlador coordinador

[33mcommit d67c91f0544490df0f2b0b00cda1fb48a003a7d9[m
Author: Dario Jofre <dario.jofrev@gmail.com>
Date:   Thu Jun 29 13:32:24 2017 -0400

    Se crea plantilla de navbar, labels y text básico, junto a titulos

[33mcommit 10b4ad81a596535bf03b73836249e82728178839[m
Author: vagrant <vagrant@homestead>
Date:   Thu Jun 29 17:26:57 2017 +0000

    Se creo una migracion y un controllar de institucion, en desarrollo

[33mcommit 0f5c294f0baae208e9030f0180b8d2f567efbeae[m
Author: Rodrigo <rodrigohax@gmail.com>
Date:   Thu Jun 29 17:00:03 2017 +0000

    Creacion modelo coordinador

[33mcommit 291a00fa8293b3fd2e729161802714d6a73442ff[m
Author: Rodrigo <rodrigohax@gmail.com>
Date:   Thu Jun 29 16:48:26 2017 +0000

    Creacion migracion coordinadores

[33mcommit 795cb10c9a405f3c0e0369918d098b481fe1d5f9[m
Author: nicoyarce <nicoyarce@gmail.com>
Date:   Wed Jun 28 03:22:25 2017 +0000

    Modelo y migraciones de actividades listas, comienzo de crud
    
    Arreglo de commit por error de autor (considero a vagrant)

[33mcommit ab90f3b820ff986fa2c72c7c4fbeb154995184b0[m
Author: Karla Leal <karla.leal1401@alumnos.ubiobio.cl>
Date:   Fri Jun 23 13:57:12 2017 -0400

    Crear migraciones de las tablas estado convenio, coordinador convenio y convenio

[33mcommit c293fc8664717198d7d04d670b6a71fbe38e5e29[m
Author: Vagrant Default User <vagrant@wheezy.raw>
Date:   Tue Jun 20 16:16:46 2017 +0000

    primer commit
