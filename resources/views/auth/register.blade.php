@extends('welcome')

@section('contenido')
<div class="principal">

    <form class="formulario" role="form" method="POST" action="{{ url('/register') }}">
        {{ csrf_field() }}

                @if ($errors->has())
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
            </div>
        @endif
        @if(Session::get('mensaje'))
            <div class='alert alert-success'>
                <p><strong>{!! Session::get('mensaje') !!}</strong></p>
            </div>
        @endif

        <div class="form-2">
            <label for="rut" class="form-label">Rut</label>
            <input id="rut" type="text" class="form-text" name="rut" value="{{ old('rut') }}">
        </div>
        <div class="form-2">
            <label for="name" class="form-label">Nombre</label>
            <input id="name" type="text" class="form-text" name="name" value="{{ old('name') }}">
        </div>
        <div class="form-2">
            <label for="apellido" class="form-label">Apellido</label>
            <input id="apellido" type="text" class="form-text" name="apellido" value="{{ old('apellido') }}">
        </div>
        <div class="form-2">
            <label for="email" class="form-label">Email</label>
            <input id="email" type="text" class="form-text" name="email" value="{{ old('email') }}">
        </div>

        <div class="form-group">
            <label for="rol" class="form-label">Rol</label>
            <input type="radio" name="rol" class="control-label" value="administrador" checked> Administrador
            <input type="radio" name="rol" class="control-label" value="coordinador"> Coordinador
            <input type="radio" name="rol" class="control-label" value="usuario"> Usuario  
        </div>

        <div class="form-2">
            <label for="password" class="form-label">Contraseña</label>
            <input id="password" type="password" class="form-text" name="password">
        </div>
        <div class="form-2">
            <label for="password-confirm" class="form-label">Confirme Contraseña</label>   
            <input id="password-confirm" type="password" class="form-text" name="password_confirmation">
        </div>
        <div class="form-group">

            <button type="submit" class="btn-submit">
                <i class="fa fa-btn fa-user"></i> Register
            </button>

        </div>
    </form>
</div>
@endsection

