<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModalidadConvenio extends Model
{
    protected $table = 'modalidadConvenios';
    protected $fillable = ['id','id_convenio','nombre_modalidad_convenio','descripcion',
							'vigente'];
}
