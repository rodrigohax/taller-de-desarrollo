@extends('welcome')
@section('titulo','Información de convenio')
@section('contenido')

<div class="principal">
            <h1 class="titleform">Información de convenio</h1>
            <div class="linetitle"></div>
            {{ Form::open(['class'=>'formulario']) }}
            <div class="form-2">
                {{ Form::label('nombre_convenio', 'Nombre',['class'=>'form-label']) }}
                {{ Form::label('nombre_convenio', $convenio->nombre_convenio, ['class' => 'form-text'])}}
            </div>
            <div class="form-2">
                {{ Form::label('fecha_inicio', 'Fecha inicio',['class'=>'form-label']) }}
                {{ Form::label('fecha_inicio', $convenio->fecha_inicio)}}
            </div>
            <div class="form-2">
                {{ Form::label('fecha_fin', 'Fecha termino',['class'=>'form-label']) }}
                @if($convenio->fecha_fin==null || $convenio->fecha_fin=='0000-00-00')
                {{ Form::label('fecha_fin',  '-', ['class' => 'form-text']) }}
                @else
                {{ Form::label('fecha_fin',  date("d/m/Y",strtotime($convenio->fecha_fin)), ['class' => 'form-text']) }}
                @endif                
            </div>
            <div class="form-2">
                {{ Form::label('fecha_firma', 'Fecha de firma',['class'=>'form-label']) }}
                {{ Form::label('fecha_firma', $convenio->fecha_firma) }}
            </div>
            <div class="form-2">
                {{ Form::label('fecha_decreto', 'Fecha de decreto',['class'=>'form-label']) }}
                {{ Form::label('fecha_decreto', $convenio->fecha_decreto) }}
            </div>
            <div class="form-2">
                {{ Form::label('numero_decreto', 'Numero de decreto',['class'=>'form-label']) }}
                {{ Form::label('numero_decreto', $convenio->numero_decreto) }}
            </div>
            <div class="form-2">
                {{ Form::label('descripcion', 'Descripcion',['class'=>'form-label']) }}
                {{ Form::label('descripcion', $convenio->descripcion, ['class' => 'form-text'])}}
            </div>
            <div class="form-2">
                {{ Form::label('vigente', 'Vigente',['class'=>'form-label']) }}
                @if($convenio->vigente==0)
                {{ Form::label('vigente', 'No',['class'=>'form-label'])}}  
                @else
                {{ Form::label('vigente', 'Si',['class'=>'form-label'])}} 
                @endif                               
            </div>
            <div class="form-2">
                {{ Form::label('vigencia', 'Vigencia',['class'=>'form-label']) }}
                {{ Form::label('vigencia', $convenio->vigencia)}}
            </div>
            <br>


            <div class="form-2">
                {{ Form::label('modalidad', 'Modalidad',['class'=>'form-label']) }}
                {{ Form::label('modalidad', $convenio->id_tipo_convenio)}}
            </div>

            <div class="form-2">
                {{ Form::label('estado', 'Estado',['class'=>'form-label']) }}
                {{ Form::label('estado', $convenio->id_estado_convenio)}}
            </div>
                        <table class="table" id="instituciones">
                    <thead>
                        <tr class="encabezado-tabla">
                            <th class="td-tabla">Instituciones vinculadas a convenio</th>     
                     
                        </tr>
                    </thead>
                    <tbody>
                            @foreach($convenio_institucion as $institucion)
                            <tr>
                                <td class="td-tabla">{{$institucion->nombre_institucion}}</td>
                            </tr>
                            @endforeach
                    
                    </tbody>
            </table>
</div>
@endsection