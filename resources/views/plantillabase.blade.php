    <div class="principal">
            
            <div class="div-group-3-btn">
                <a href="" class="btn-principal">Botón 1</a>
                <a href="" class="btn-principal">Botón 2</a>
                <a href="" class="btn-principal">Botón 3</a>
            </div>
            <h1 class="titleform">Titulo</h1>
            <div class="linetitle"></div>
            
            <form class="formulario">
                @yield('contenido')
                <div class="form-2">
                    <label class="form-label">Label 1</label>
                    <input type="text" name="text1" class="form-text">
                </div>
                <div class="form-2">
                    <label class="form-label">Label 2</label>
                    <input type="text" name="text2" class="form-text">
                </div>
                <div class="form-2">
                    <label class="form-label">Label Dropdown</label>
                    <select class="form-select" name="select">
                        <option disabled selected value>-- Seleccione una opción --</option>
                        <option>Opción 1</option>
                        <option>Opción 2</option>
                        <option>Opción 3</option>
                    </select>
                </div>
                <br>
                <div class="div-1-btn-center">
                    <button type="submit" class="btn-submit">Botón submit</sub>
                </div>

                <div class="div-btn-center">

                    {{Form::submit('Registrar', ['class'=>'form-btn-submit'])}}
                    
                </div>
            </form>


        </div>