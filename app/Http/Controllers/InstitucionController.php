<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use App\Institucion;
use App\Http\Requests;
use App\Http\Requests\InstitucionRequest;


use Illuminate\Support\Facades\Session;

class InstitucionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $instituciones = Institucion::orderBy('id', 'ASC')->paginate(100);
        return view('institucion.index')->with('instituciones',$instituciones);
    }


    public function create()
    {
        return view('institucion.create');
    }


    public function store(InstitucionRequest $request)
    {

        $institucion = new Institucion();
        $institucion->rut_institucion = $request->input("rut_institucion");
        $institucion->nombre_institucion = $request->input("nombre_institucion");
        $institucion->email_institucion = $request->input("email_institucion");
        $institucion->direccion_institucion= $request->input("direccion_institucion");
        $institucion->save();
        Session::flash('mensaje','La Institución se ha almacenado Correctamente');
        return redirect()->to('/institucion');
    }


    public function show($id)
    {
        $institucion = Institucion::find($id);

            return view('institucion.show',compact('institucion',$institucion));


    }


    public function edit($id)
    {
        $institucion = Institucion::find($id);

        return view('institucion.edit',compact('institucion',$institucion));
    }

    public function update(InstitucionRequest $request, $id)
    {
        $institucion= Institucion::find($id);
        $institucion->fill($request->all());
        $institucion->save();
       Session::flash('mensaje','Se ha Actualizado la Institución Correctamente');
        return redirect()->route('institucion.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $institucion = Institucion::find($id)->delete();
        Session::flash('mensaje',"La Institución fue eliminada correctamente");
        return redirect()->to('institucion');
    }
}
