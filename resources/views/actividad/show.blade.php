@extends('welcome')
@section('titulo','Información de actividad')
@section('contenido')

<div class="principal">
            <h1 class="titleform">Información de actividad</h1>
            <div class="linetitle"></div>
        {{ Form::open(['class'=>'formulario']) }}
            <div class="form-2">
                {{ Form::label('nombre_actividad', 'Nombre',['class'=>'form-label']) }}
                {{ Form::label('nombre_actividad', $actividad->nombre_actividad, ['class' => 'form-text'])}}
            </div>
            <div class="form-2">
                {{ Form::label('descripcion', 'Descripcion',['class'=>'form-label']) }}
                {{ Form::label('descripcion',  $actividad->descripcion, ['class' => 'form-text'])}}
            </div>
            <div class="form-2">
                {{ Form::label('fecha_inicio', 'Fecha inicio',['class'=>'form-label']) }}
                {{ Form::label('fecha_inicio',  date("d/m/Y",strtotime($actividad->fecha_inicio)), ['class' => 'form-text'])}}
            </div>
                        
            <div class="form-2">
                {{ Form::label('fecha_fin', 'Fecha termino',['class'=>'form-label']) }}
                @if($actividad->fecha_fin==null || $actividad->fecha_fin=='0000-00-00')
                {{ Form::label('fecha_fin',  '-', ['class' => 'form-text']) }}
                @else
                {{ Form::label('fecha_fin',  date("d/m/Y",strtotime($actividad->fecha_fin)), ['class' => 'form-text']) }}
                @endif
                
            </div>

         
         
</div>
@endsection