@extends('welcome')
@section('titulo','Datos de coordinador ')
@section('contenido')
<div class="principal">
            <h1 class="titleform">Datos de coordinador {{$coordinador->nombre}}</h1>
            <div class="linetitle"></div>
        {{ Form::open(['route' => ['coordinador.update',$coordinador],  'method' => 'PUT','class'=>'formulario']) }}
            <div class="form-2">
                {{ Form::label('rut', 'Rut',['class'=>'form-label']) }}
                {{ Form::text('rut', $coordinador->rut, ['class' => 'form-text'])}}
            </div>
            <div class="form-2">
                {{ Form::label('nombre', 'Nombre',['class'=>'form-label']) }}
                {{ Form::text('nombre', $coordinador->nombre, ['class' => 'form-text'])}}
            </div>
            <div class="form-2">
                {{ Form::label('unidad_academica', 'Unidad Académica: ',['class'=>'form-label']) }}
                {{ Form::text('unidad_academica', $coordinador->unidad_academica, ['class' => 'form-text'])}}
            </div>
            <div class="form-2">
                {{ Form::label('email', 'Email',['class'=>'form-label']) }}
                {{ Form::email('email', $coordinador->email, ['class' => 'form-text'])}}
            </div>
            <div class="form-2">
                {{ Form::label('fecha_inicio', 'Fecha inicio',['class'=>'form-label']) }}
                {{ Form::date('fecha_inicio', $coordinador->fecha_inicio, ['class' => 'form-text'])}}
            </div>
                        
            <div class="form-2">
                {{ Form::label('fecha_fin', 'Fecha término',['class'=>'form-label']) }}
                {{ Form::date('fecha_fin', $coordinador->fecha_fin, ['class' => 'form-text']) }}
            </div>
            <div class="form-2">
                {{Form::label('esExterno','Externo:', ['class'=>'form-label'])}}
                <div class="radiobuttons">
                    <div class="rb">
                        @if($coordinador->esExterno==true)
                        {{Form::radio('esExterno','Sí',['value'=>'Sí'])}}
                        <label for="radio01"><span></span>Sí</label>        
                        @else
                        {{Form::radio('esExterno','Sí')}}
                        <label for="radio01"><span></span>Sí</label>        
                        @endif
                    </div>
                    <div class="rb">
                        @if($coordinador->esExterno==false)
                        {{Form::radio('esExterno','Sí',['value'=>'No'])}}
                        <label for="radio01"><span></span>No</label>        
                        @else
                        {{Form::radio('esExterno','No')}}
                        <label for="radio01"><span></span>No</label>        
                        @endif  
                    </div>
                </div>
            </div>
            <div class="form-group">
                {{ Form::submit('Guardar',['class'=>'btn-submit']) }}
            </div>
                    {{ Form::close()}}
                </div>
            </div>
  
</div>
@endsection
