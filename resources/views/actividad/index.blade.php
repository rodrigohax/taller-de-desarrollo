@extends('welcome')
@section('contenido')
<div class="principal">            
     
            <a href="{{ route('actividad.create') }}" class="btn-principal" style="float: right;">Agregar actividad</a>
           <h1 class="titleform" style=" margin-top: 1%;">Actividades</h1>
            <div class="linetitle" style=" margin-top: 2%;"></div>

    <br><br>
    <table class="table">
                    <thead>
                        <tr class="encabezado-tabla">
                            <th>ID</th>     
                            <th>Fecha inicio</th>
                            <th>Fecha termino</th>
                            <th>Nombre actividad</th>
                            <th>Vigencia</th>                 
                            <th></th>   
                            <th></th>   
                            <th></th>   
                        </tr> 
                    </thead>
                    <tbody>
                    @if(count($actividades)>0)
                        @foreach($actividades as $actividad)
                            <tr class="table-color">
                                <td class="td-tabla">{{$actividad->id}}</td>
                                <td class="td-tabla">{{date("d/m/Y",strtotime($actividad->fecha_inicio))}}</td>
                                @if($actividad->fecha_fin==null || $actividad->fecha_fin=='0000-00-00')
                                    <td class="td-tabla"> -</td>
                                @else
                                    <td class="td-tabla">{{date("d/m/Y",strtotime($actividad->fecha_fin))}}</td>
                                @endif
                                <td class="td-tabla">{{$actividad->nombre_actividad}}</td>
                                @if($actividad->vigente==1)
                                    <td class="td-tabla">Sí</td>
                                @else
                                    <td class="td-tabla">No</td>
                                @endif

                                <td class="td-tabla"><a href="{{ route('actividad.show', $actividad->id) }}" class="btn btn-info">
                                    <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>

                                <td class="td-tabla"><a href="{{ route('actividad.edit', $actividad->id) }}" class="btn btn-warning">
                                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>

                                <td class="td-tabla"><a href="{{ route('actividad.destroy', $actividad->id) }}" class="btn btn-danger" onclick="return confirm('Eliminar')"> <span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>
                            </tr>
                            @endforeach
                    @else
                    <div class="tabla-vacia">
                        <p>Sin actividades</p>
                    </div>
                    
                    @endif
                    </tbody>
        </table>

                        
                    
       
</div>
@endsection
