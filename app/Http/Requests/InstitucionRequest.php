<?php


namespace App\Http\Requests;

use App\Http\Requests\Request;

class InstitucionRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                    "nombre_institucion"=> "required|min:3|max:50",
                    "rut_institucion"=> "required|min:6|max:15",
                    "direccion_institucion"=> "required|min:10|max:50",
                    "email_institucion"=>"required|min:10|max:30",
                ];
    }
    public function messages()
    {
        return [
            'nombre_institucion.required' =>'El nombre es requerida',
            'nombre_institucion.min' =>'El nombre debe contener al menos 3 caracteres',
            'nombre_institucion.max' => 'El nombre debe contener un maximo de 50 caracteres',
            'rut_institucion.required' => 'El rut es obligatorio',    
            'rut_institucion.min' => 'El rut debe tener 6 digitos minimo',      
            'direccion_institucion.required'=>'La direccion es requerida',
            'direccion_institucion.min'=>'La direccion debe tener un minimo de 10 caracteres',
            'direccion_institucion.max'=>'La direccion puede tener un maximo de 50 caracteres',
            'email_institucion.required'=>'El email es requerido',
            'email_institucion.min'=>'La email debe tener un minimo de 10 caracteres',
            'email_institucion.max'=>'La email puede tener un maximo de 30 caracteres'
        ];
    }
}