@extends('welcome')
@section('contenido')
<div class="principal">            
            
                <a href="{{ route('convenio.create') }}" class="btn-principal" style="float:right;">Agregar convenio</a>

            
            <h1 class="titleform">Convenios</h1>
            <br>
            <div class="linetitle"></div>
    <br><br>
    <table class="table">
                    <thead>
                        <tr class="encabezado-tabla">
                            <th>ID</th>     
                            <th>Fecha inicio</th>
                            <th>Fecha termino</th>
                            <th>Nombre convenio</th>
                            <th>Vigencia</th>                 
                            <th>Ver</th>   
                            <th>Editar</th>   
                            <th>Eliminar</th>   
                        </tr> 
                    </thead>
                    <tbody>
                    @if(count($convenios)>0)
                        @foreach($convenios as $convenio)
                            <tr class="table-color">
                                <td class="td-tabla">{{$convenio->id}}</td>
                                <td class="td-tabla">{{date("d/m/Y",strtotime($convenio->fecha_inicio))}}</td>
                                @if($convenio->fecha_fin==null || $convenio->fecha_fin=='0000-00-00')
                                    <td class="td-tabla"> -</td>
                                @else
                                    <td class="td-tabla">{{date("d/m/Y",strtotime($convenio->fecha_fin))}}</td>
                                @endif
                                <td class="td-tabla">{{$convenio->nombre_convenio}}</td>
                                @if($convenio->vigente==1)
                                    <td class="td-tabla">Sí</td>
                                @else
                                    <td class="td-tabla">No</td>
                                @endif

                                <td class="td-tabla"><a href="{{ route('convenio.show', $convenio->id) }}" class="btn btn-info">
                                    <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>

                                <td class="td-tabla"><a href="{{ route('convenio.edit', $convenio->id) }}" class="btn btn-warning">
                                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>

                                <td class="td-tabla"><a href="{{ route('convenio.destroy', $convenio->id) }}" class="btn btn-danger" onclick="return confirm('Eliminar')"> <span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>
                            </tr>
                            @endforeach
                    @else
                    <div class="tabla-vacia">
                        <p>Sin convenios</p>
                    </div>
                    
                    @endif
                    </tbody>
        </table>

</div>
@endsection
