<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Actividad;

class ActividadController extends Controller
{
        public function __construct()
    {

        $this->middleware('cooryadm', ['only' => [
            'index',
        ]]);

        $this->middleware('administrador', ['only' => [
            'edit',
            'destroy',
            'create',
        ]]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $actividad = Actividad::orderBy('id', 'ASC')->paginate(5);
        return view('actividad.index')->with('actividades',$actividad);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('actividad.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $actividad = new Actividad($request->all());
        if($request->fecha_fin==null){
            $actividad->vigente=1;
        }
        $actividad->save();
        //Flash::success("Se ha registrado el colegio " . $colegio->nombre);
        return redirect()->route('actividad.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $actividad = Actividad::find($id);
          return view('actividad.show')->with('actividad', $actividad);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $actividad = Actividad::find($id);
        return view('actividad.edit')->with('actividad', $actividad);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $actividad= Actividad::find($id);
        $actividad->fill($request->all());
        $actividad->save();
        return redirect()->route('actividad.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Actividad::destroy($id);
        return redirect()->route('actividad.index');
    }
}
