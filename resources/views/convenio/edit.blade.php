@extends('welcome')
@section('titulo','Edición de convenio')

@section('contenido')

<div class="principal">
            <h1 class="titleform">Edición información de convenio</h1>
            <div class="linetitle"></div>
        {{ Form::open(['route' => ['convenio.update',$convenio],  'method' => 'PUT','class'=>'formulario']) }}            
            <div class="form-2">
                {{ Form::label('nombre_convenio', 'Nombre',['class'=>'form-label']) }}
                {{ Form::text('nombre_convenio', $convenio->nombre_convenio, ['class' => 'form-text'])}}
            </div>    

            <div class="form-2">
                {{ Form::label('fecha_inicio', 'Fecha inicio',['class'=>'form-label']) }}
                {{ Form::date('fecha_inicio', $convenio->fecha_inicio)}}
            </div>  

            <div class="form-2">
                {{ Form::label('fecha_fin', 'Fecha termino',['class'=>'form-label']) }}
                {{ Form::date('fecha_fin', $convenio->fecha_fin)}}
            </div>

            <div class="form-2">
                {{ Form::label('fecha_firma', 'Fecha de firma',['class'=>'form-label']) }}
                {{ Form::date('fecha_firma', $convenio->fecha_firma) }}
            </div> 

            <div class="form-2">
                {{ Form::label('fecha_decreto', 'Fecha de decreto',['class'=>'form-label']) }}
                {{ Form::date('fecha_decreto', $convenio->fecha_decreto) }}
            </div>

            <div class="form-2">
                {{ Form::label('numero_decreto', 'Numero de decreto',['class'=>'form-label']) }}
                {{ Form::text('numero_decreto', $convenio->numero_decreto, ['class'=>'form-text','onkeypress'=>'return soloNumeros(event)'])  }}
            </div>

            <div class="form-2">
                {{ Form::label('descripcion', 'Descripcion',['class'=>'form-label']) }}
                {{ Form::text('descripcion', $convenio->descripcion, ['class' => 'form-text'])}}
            </div>

            <div class="form-2">
                {{ Form::label('vigente', 'Vigente',['class'=>'form-label']) }}
                {{ Form::select('vigente', ['1' => 'Si', '0' => 'No'], $convenio->vigente, ['class' => 'form-text', 'placeholder' => 'Seleccione una opción']) }}                
            </div>

            <div class="form-2">
                {{ Form::label('vigencia', 'Vigencia',['class'=>'form-label']) }}
                {{ Form::text('vigencia', $convenio->vigencia,['class'=>'form-text','onkeypress'=>'return soloNumeros(event)']) }}
            </div>
                 <a id="bt_add" class="btn-principal" style="float:right;">+ Agregar Institución</a>  
            <table class="table" id="instituciones">
                    <thead>
                        <tr class="encabezado-tabla">
                            <th class="td-tabla">Instituciones vinculadas a convenio</th>     
                            <th class="td-tabla"></th>     
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
            </table>

            <div class="form-2">
                {{ Form::label('modalidad', 'Modalidad',['class'=>'form-label']) }} 
                {{ Form::select('idModalidades', $modalidades, $convenio->id_tipo_convenio, ['class' => 'form-text', 'placeholder' => 'Seleccione una opción']) }} 
            </div>         
           
            <div class="form-2">
                {{ Form::label('estado', 'Estado',['class'=>'form-label']) }} 
                {{ Form::select('idEstados', $estados, $convenio->id_estado_convenio, ['class' => 'form-text', 'placeholder' => 'Seleccione una opción']) }} 
            </div>

            <div class="form-group">
                {{ Form::submit('Guardar',['class'=>'btn-submit']) }}
            </div>
                    {{ Form::close()}}
                </div>
            </div>
            <br>


</div>
<script type="text/javascript">
                function soloNumeros(e){
                    var keynum = window.event ? window.event.keyCode : e.which;
                    if ((keynum == 8) || (keynum == 46))
                    return true;
                     
                    return /\d/.test(String.fromCharCode(keynum));
                }
            </script>  
<script type="text/javascript">
        
    $(document).ready(function(){
        agregarPredeterminadas();
        $('#bt_add').click(function(){
            agregarInstituciones();
        });

    });
    var cont=0;
    function agregarInstitucionesPredeterminadas(){
        
        @foreach($convenio_institucion as $ee)
        cont++; 
            var fila='<tr id="fila'+cont+'"> <td class="td-tabla">{{Form::select("id_instituciones[]",$instituciones,$ee->id)}}</td><td class="td-tabla"><a onClick="eliminar('+cont+')" id="bt_del" class="btn btn-danger"> <span id="bt_del" class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td></tr>';
                $('#instituciones').append(fila);
        @endforeach
    }
    function agregarInstituciones(){
        cont ++;
        var fila='<tr id="fila'+cont+'"><td class="td-tabla">{{Form::select("id_instituciones[]",$instituciones,null)}}</td><td class="td-tabla"><a onClick="eliminar('+cont+')" id="bt_del" class="btn btn-danger"> <span id="bt_del" class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td></tr>';
        $('#instituciones').append(fila);
    }
    function eliminar(id_fila){

        $('#fila'+id_fila).remove();
    }
</script>
@endsection
