@extends('welcome')
@section('titulo','Registro de instituciones')

@section('contenido')

    @if ($errors->has())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <p>{{ $error }}</p>
            @endforeach
        </div>
    @endif
    @if(Session::get('mensaje'))
        <div class='alert alert-success'>
            <p><strong>{!! Session::get('mensaje') !!}</strong></p>
        </div>
    @endif

    <div class="principal">
        <h1 class="titleform">Edición información de institucion</h1>
        <div class="linetitle"></div>
        {{ Form::open(['route' => ['institucion.update',$institucion],  'method' => 'PUT','class'=>'formulario']) }}
        <div class="form-2">
            {{ Form::label('nombre_institucion', 'Nombre',['class'=>'form-label']) }}
            {{ Form::text('nombre_institucion', $institucion->nombre_institucion, ['class' => 'form-text'])}}
        </div>
        <div class="form-2">
            {{ Form::label('rut_institucion', 'Rut',['class'=>'form-label']) }}
            {{ Form::text('rut_institucion',  $institucion->rut_institucion, ['class' => 'form-text'])}}
        </div>
        <div class="form-2">
            {{ Form::label('descripcion', 'Descripcion',['class'=>'form-label']) }}
            {{ Form::text('descripcion',  $institucion->descripcion, ['class' => 'form-text'])}}
        </div>
        <div class="form-2">
            {{ Form::label('email_institucion', 'Email',['class'=>'form-label']) }}
            {{ Form::text('email_institucion',  $institucion->email_institucion, ['class' => 'form-text'])}}
        </div>

        <div class="form-group">
            {{ Form::submit('Guardar cambios',['class'=>'btn-submit']) }}
        </div>
        {{ Form::close()}}
    </div>
    </div>

    </div>
@endsection