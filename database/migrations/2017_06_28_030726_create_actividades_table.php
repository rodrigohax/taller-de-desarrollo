<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActividadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actividades', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('id_estado_actividad')->unsigned()->index();
            $table->integer('id_tipo_actividad')->unsigned()->index();
            $table->integer('id_responsable_actividad')->unsigned()->index();
            $table->integer('id_convenio')->unsigned()->index();
            $table->date('fecha_inicio');
            $table->date('fecha_fin');
            $table->string('nombre_actividad');
            $table->string('descripcion');
            $table->boolean('vigente');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('actividades');
    }
}
