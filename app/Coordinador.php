<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coordinador extends Model
{
    protected $table = "coordinadores";
    protected $fillable = ['rut','nombre','fecha_inicio','fecha_fin','vigente','exEsterno','unidad_academica','email'];
}
