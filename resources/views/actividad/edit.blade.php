@extends('welcome')
@section('titulo','Registro de actividad')

@section('contenido')

<div class="principal">
            <h1 class="titleform">Edición información de actividad</h1>
            <div class="linetitle"></div>
        {{ Form::open(['route' => ['actividad.update',$actividad],  'method' => 'PUT','class'=>'formulario']) }}
            <div class="form-2">
                {{ Form::label('nombre_actividad', 'Nombre',['class'=>'form-label']) }}
                {{ Form::text('nombre_actividad', $actividad->nombre_actividad, ['class' => 'form-text'])}}
            </div>
            <div class="form-2">
                {{ Form::label('descripcion', 'Descripcion',['class'=>'form-label']) }}
                {{ Form::text('descripcion',  $actividad->descripcion, ['class' => 'form-text'])}}
            </div>
            <div class="form-2">
                {{ Form::label('fecha_inicio', 'Fecha inicio',['class'=>'form-label']) }}
                {{ Form::date('fecha_inicio',  $actividad->fecha_inicio, ['class' => 'form-text'])}}
            </div>
                        
            <div class="form-2">
                {{ Form::label('fecha_fin', 'Fecha termino',['class'=>'form-label']) }}
                {{ Form::date('fecha_fin',  $actividad->fecha_fin, ['class' => 'form-text']) }}
            </div>

            <div class="form-group">
                {{ Form::submit('Guardar cambios',['class'=>'btn-submit']) }}
            </div>
                    {{ Form::close()}}
                </div>
            </div>
  
</div>
@endsection