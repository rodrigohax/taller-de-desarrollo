<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadoConvenio extends Model
{
    protected $table = 'estadoConvenios';
    protected $fillable = ['id','nombre_estado_convenio','descripcion',
							'vigente'];
}
