@extends('welcome')
@section('titulo','Registrar estado de convenio')

@section('contenido')

<div class="principal">
            <h1 class="titleform">Registrar estado de convenio</h1>
            <div class="linetitle"></div>

            {{ Form::open(['route' => 'estado.store',  'method' => 'POST','class'=>'formulario']) }}
            <div class="form-2">
                {{ Form::label('nombre_estado_convenio', 'Nombre de estado',['class'=>'form-label']) }}
                {{ Form::text('nombre_estado_convenio', null, ['class' => 'form-text'])}}
            </div>    

            <div class="form-2">
                {{ Form::label('descripcion', 'Descripcion',['class'=>'form-label']) }}
                {{ Form::text('descripcion', null, ['class' => 'form-text'])}}
            </div>

            <div class="form-2">
                {{ Form::label('vigente', 'Vigente',['class'=>'form-label']) }} 
                {{ Form::select('vigente', ['1' => 'Si', '0' => 'No'], null, ['class' => 'form-text', 'placeholder' => 'Seleccione una opción']) }} 
            </div>

            <div class="form-group">
                {{ Form::submit('Guardar',['class'=>'btn-submit']) }}
            </div>

            {{ Form::close()}}
            </div>
            </div>

</div>
@endsection