@extends('welcome')
@section('titulo','Registro de actividad')

@section('contenido')

<div class="principal">
            <h1 class="titleform">Registro de actividad</h1>
            <div class="linetitle"></div>
        {{ Form::open(['route' => 'actividad.store',  'method' => 'POST','class'=>'formulario']) }}
            <div class="form-2">
                {{ Form::label('nombre_actividad', 'Nombre',['class'=>'form-label']) }}
                {{ Form::text('nombre_actividad', null, ['class' => 'form-text'])}}
            </div>
            <div class="form-2">
                {{ Form::label('descripcion', 'Descripcion',['class'=>'form-label']) }}
                {{ Form::text('descripcion', null, ['class' => 'form-text'])}}
            </div>
            <div class="form-2">
                {{ Form::label('fecha_inicio', 'Fecha inicio',['class'=>'form-label']) }}
                {{ Form::date('fecha_inicio', null, ['class' => 'form-text'])}}
            </div>
                        
            <div class="form-2">
                {{ Form::label('fecha_fin', 'Fecha termino',['class'=>'form-label']) }}
                {{ Form::date('fecha_fin', null, ['class' => 'form-text']) }}
            </div>

            <div class="form-group">
                {{ Form::submit('Guardar',['class'=>'btn-submit']) }}
            </div>
                    {{ Form::close()}}
                </div>
            </div>
  
</div>
@endsection