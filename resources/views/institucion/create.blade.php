@extends('welcome')
@section('titulo','Registro de institucion')

@section('contenido')

    <div class="principal">
        <h1 class="titleform">Registro de institucion</h1>
        <div class="linetitle"></div>
        {{ Form::open(['route' => 'institucion.store',  'method' => 'POST','class'=>'form-horizontal']) }}

        @if ($errors->has())
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
            </div>
        @endif
        @if(Session::get('mensaje'))
            <div class='alert alert-success'>
                <p><strong>{!! Session::get('mensaje') !!}</strong></p>
            </div>
        @endif


        <div class="form-2">
            {{ Form::label('nombre_institucion', 'Nombre',['class'=>'form-label']) }}
            {{ Form::text('nombre_institucion', null, ['class' => 'form-text'])}}
        </div>
        <div class="form-2">
            {{ Form::label('vigente', 'Estado',['class'=>'form-label']) }}

                <select name="vigencia" id="vigencia" class="form-select">
                    <option value="">Seleccione un Opción</option>
                    <option value="vigente">Vigente</option>
                    <option value="Novigente">No Vigente</option>
                </select>

        <div class="form-2">
            {{ Form::label('rut_institucion', 'Rut institucion',['class'=>'form-label']) }}
            {{ Form::text('rut_institucion', null, ['class' => 'form-text'])}}
        </div>

        <div class="form-2">
            {{ Form::label('direccion_institucion', 'Direccion institucion',['class'=>'form-label']) }}
            {{ Form::text('direccion_institucion', null, ['class' => 'form-text'])}}
        </div>
        <div class="form-2">
            {{ Form::label('email_institucion', 'Email institucion',['class'=>'form-label']) }}
            {{ Form::text('email_institucion', null, ['class' => 'form-text'])}}
        </div>

        <div class="form-group">
            {{ Form::submit('Guardar',['class'=>'btn-submit']) }}
        </div>
        {{ Form::close()}}
    </div>
    </div>

    </div>
@endsection