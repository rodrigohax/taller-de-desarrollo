@extends('welcome')
@section('titulo','Registro de convenio')

@section('contenido')

<div class="principal">
            <h1 class="titleform">Registro de convenio</h1>
            <div class="linetitle"></div>
        {{ Form::open(['route' => 'convenio.store',  'method' => 'POST','class'=>'formulario']) }}
            <div class="form-2">
                {{ Form::label('nombre_convenio', 'Nombre',['class'=>'form-label']) }}
                {{ Form::text('nombre_convenio', null, ['class' => 'form-text'])}}
            </div>    

            <div class="form-2">
                {{ Form::label('fecha_inicio', 'Fecha inicio',['class'=>'form-label']) }}
                {{ Form::date('fecha_inicio', \Carbon\Carbon::now())}}
            </div>  

            <div class="form-2">
                {{ Form::label('fecha_fin', 'Fecha termino',['class'=>'form-label']) }}
                {{ Form::date('fecha_fin', \Carbon\Carbon::now())}}
            </div>

            <div class="form-2">
                {{ Form::label('fecha_firma', 'Fecha de firma',['class'=>'form-label']) }}
                {{ Form::date('fecha_firma', \Carbon\Carbon::now()) }}
            </div>

            <div class="form-2">
                {{ Form::label('fecha_decreto', 'Fecha de decreto',['class'=>'form-label']) }}
                {{ Form::date('fecha_decreto', \Carbon\Carbon::now()) }}
            </div>

            <div class="form-2">
                {{ Form::label('numero_decreto', 'Numero de decreto',['class'=>'form-label']) }}
                {{ Form::text('numero_decreto', null, ['class'=>'form-text','onkeypress'=>'return soloNumeros(event)']) }}
            </div>

            <div class="form-2">
                {{ Form::label('descripcion', 'Descripcion',['class'=>'form-label']) }}
                {{ Form::text('descripcion', null, ['class' => 'form-text'])}}
            </div>

            <div class="form-2">
                {{ Form::label('vigente', 'Vigente',['class'=>'form-label']) }} 
                {{ Form::select('vigente', ['1' => 'Si', '0' => 'No'], null, ['class' => 'form-text', 'placeholder' => 'Seleccione una opción']) }} 
            </div>

            <div class="form-2">
                {{ Form::label('vigencia', 'Vigencia',['class'=>'form-label']) }}
                {{ Form::text('vigencia', null, ['class'=>'form-text','onkeypress'=>'return soloNumeros(event)']) }}
            </div>
          <div class="form-2">
                {{ Form::label('modalidad', 'Modalidad',['class'=>'form-label']) }} 
                {{ Form::select('idModalidades', $modalidades, null, ['class' => 'form-text', 'placeholder' => 'Seleccione una opción']) }} 
            </div>         
           
            <div class="form-2">
                {{ Form::label('estado', 'Estado',['class'=>'form-label']) }} 
                {{ Form::select('idEstados', $estados, null, ['class' => 'form-text', 'placeholder' => 'Seleccione una opción']) }} 
            </div>
            
            <a id="bt_add" class="btn-principal" style="float:right;">+ Agregar Institución</a>                
            <table class="table" id="instituciones">
                    <thead>
                        <tr class="encabezado-tabla">
                            <th class="td-tabla">Instituciones vinculadas a convenio</th>     
                            <th></th>
                        </tr>
                    </thead>
            </table>            
            
  
            <div class="form-group">
                {{ Form::submit('Guardar',['class'=>'btn-submit']) }}
            </div>
            {{ Form::close()}}
                </div>
            </div>
</div>
<script type="text/javascript">
    function soloNumeros(e){
        var keynum = window.event ? window.event.keyCode : e.which;
        if ((keynum == 8) || (keynum == 46))
        return true;
         
        return /\d/.test(String.fromCharCode(keynum));
    }
</script>

<script type="text/javascript">        
    $(document).ready(function(){
        $('#bt_add').click(function(){
            agregarInstituciones();
        });
    });
    var contInstituciones=0;    
    function agregarInstituciones(){
        contInstituciones ++;
        var fila='<tr id="fila'+contInstituciones+'"><td class="td-tabla">{{Form::select("id_instituciones[]",$instituciones,null)}}</td><td class="td-tabla"><a onClick="eliminar('+contInstituciones+')" id="bt_del" class="btn btn-danger"> <span id="bt_del" class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td></tr>';
        $('#instituciones').append(fila);
    }
   
    function eliminar(id_fila){
        $('#fila'+id_fila).remove();
    }
</script>
@endsection