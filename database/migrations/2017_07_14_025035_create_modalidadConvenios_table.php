<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModalidadConveniosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modalidadConvenios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_convenio')->unsigned()->index();
            $table->string('nombre_modalidad_convenio');
            $table->string('descripcion');
            $table->boolean('vigente');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('modalidadConvenios');
    }
}
