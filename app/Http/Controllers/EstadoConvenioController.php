<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\EstadoConvenio;

class EstadoConvenioController extends Controller
{
        public function __construct()
    {

        $this->middleware('auth', ['only' => [
            'index',
        ]]);

        $this->middleware('administrador', ['only' => [
            'edit',
            'destroy',
            'create',
        ]]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $estado = EstadoConvenio::orderBy('id', 'ASC')->paginate(10);
        return view('estado.index')->with('estados',$estado);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('estado.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $estado = new EstadoConvenio($request->all());       
        $estado->save();
        //Flash::success("Se ha registrado el convenio con id" . $convenio->id);
        return redirect()->route('estado.index');       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $estado = EstadoConvenio::find($id);
        return view('estado.edit')->with('estado', $estado);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $estado= EstadoConvenio::find($id);
        $estado->fill($request->all());
        $estado->save();
        return redirect()->route('estado.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        EstadoConvenio::destroy($id);
        return redirect()->route('estado.index');
    }
}
