@extends('welcome')
@section('titulo','Información de institucion')
@section('contenido')

    <div class="principal">
        <h1 class="titleform">Información de institucion</h1>
        <div class="linetitle"></div>
        {{ Form::open(['class'=>'formulario']) }}
        <div class="form-2">
            {{ Form::label('nombre_actividad', 'Nombre',['class'=>'form-label']) }}
            {{ Form::label('nombre_actividad', $institucion->nombre_actividad, ['class' => 'form-text'])}}
        </div>
        <div class="form-2">
            {{ Form::label('vigente', 'Estado vigencia',['class'=>'form-label']) }}
            {{ Form::label('vigente', $institucion->vigente, ['class' => 'form-text'])}}
        </div>
        <div class="form-2">
            {{ Form::label('rut_institucion', 'Rut de institucion',['class'=>'form-label']) }}
            {{ Form::label('rut_institucion',  $institucion->rut_institucion, ['class' => 'form-text'])}}
        </div>
        <div class="form-2">
            {{ Form::label('direccion_institucion', 'Direccion de institucion',['class'=>'form-label']) }}
            {{ Form::label('direccion_institucion',  $institucion->direccion_institucion, ['class' => 'form-text'])}}
        </div>
        <div class="form-2">
            {{ Form::label('email_institucion', 'Correo de institucion',['class'=>'form-label']) }}
            {{ Form::label('email_institucion',  $institucion->email_institucion, ['class' => 'form-text'])}}
        </div>

    </div>
@endsection