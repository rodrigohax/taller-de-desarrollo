<!DOCTYPE html>

<html>
    <head>
        <title>@yield('titulo','Inicio')</title>
        <link rel="stylesheet" type="text/css" href="{{asset('estilos/estilos.css')}}">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css">
        <meta charset="utf-8">
    </head>
    <body topmargin="0" class="body">
        <nav class="navbar">
            <ul>
                <li><a href="{{route('convenio.index')}}">Convenios</a></li>
                <li><a href="{{route('actividad.index')}}">Actividades</a></li>
                <li><a href="{{route('coordinador.index')}}">Coordinadores</a></li>
                <li><a href="{{route('institucion.index')}}">Institución</a></li>
                <li><a href="{{route('modalidad.index')}}">Modalidad Conv.</a></li>
                <li><a href="{{route('estado.index')}}">Estados Conv.</a></li>
                <h1 class="h1universidad">Universidad de Macondo</h1>   

                @if (Auth::guest())
                        <li class="lilogin"><a href="{{ url('/login') }}">Ingresar</a></li>
                        <li class="lilogin"><a href="{{ url('/register') }}">Registrar</a></h1>
                        @else
                        
                         <li style="float:right"><a href="{{ url('/logout') }}">Desconectar</a></li>
                        <li style="float:right"> {{ Auth::user()->name }} </li>
                        <li style="float:right"> {{ Auth::user()->rol }} </li>
             @endif
            </ul>
        </nav>
                       @if($errors->has())
                <div class='alert alert-danger'>
                    @foreach ($errors->all('<p>:message</p>') as $message)
                    {!! $message !!}
                    @endforeach
                </div>
                @endif
            <script src="{{asset('js/jquery-3.1.1.min.js')}}"></script>        
    <script src="{{asset('js/bootstrap.js')}}"></script>        

    
        @yield('contenido')
    <script type="text/JavaScript">
        function redireccion(){

        }
    </script>
    </body>

</html>