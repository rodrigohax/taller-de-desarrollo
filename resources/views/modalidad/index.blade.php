@extends('welcome')
@section('contenido')
<div class="principal">            
            <div class="div-group-3-btn">
                <a href="{{ route('modalidad.create') }}" class="btn-principal">Agregar modalidad</a>
            </div>
            <h1 class="titleform">Modalidades de convenio</h1>
            <div class="linetitle"></div>
    <br><br>
    <table class="table">
                    <thead>
                        <tr class="encabezado-tabla">
                            <th>ID</th>
                            <th>Nombre de modalidad de convenio</th>
                            <th>Descripcion</th>
                            <th>Vigente</th>
                            <th>Editar</th>   
                            <th>Eliminar</th>   
                        </tr> 
                    </thead>
                    <tbody>
                    @if(count($modalidades)>0)
                        @foreach($modalidades as $modalidad)
                            <tr class="table-color">
                                <td class="td-tabla">{{$modalidad->id}}</td>                               
                                <td class="td-tabla">{{$modalidad->nombre_modalidad_convenio}}</td>
                                <td class="td-tabla">{{$modalidad->descripcion}}</td>
                                @if($modalidad->vigente==1)
                                    <td class="td-tabla">Sí</td>
                                @else
                                    <td class="td-tabla">No</td>
                                @endif                               

                                <td class="td-tabla"><a href="{{ route('modalidad.edit', $modalidad->id) }}" class="btn btn-warning">
                                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>

                                <td class="td-tabla"><a href="{{ route('modalidad.destroy', $modalidad->id) }}" class="btn btn-danger" onclick="return confirm('Eliminar')"> <span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>
                            </tr>
                            @endforeach
                    @else
                    <div class="tabla-vacia">
                        <p>Sin modalidades</p>
                    </div>
                    
                    @endif
                    </tbody>
        </table>

</div>
@endsection
